package com.griddynamics.intership.capstone.merge;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

public class PartitionManagerTest {

    private static File currentFolder;
    private static PartitionManager manager;

    @BeforeClass
    @DisplayName("Creates partition manager in the current folder.")
    public static void createPartitionManager() {
        var currentFile = new File(FilesToList.testDirectory + "integers.random");
        currentFolder = new File(FilesToList.testDirectory + "integers.random.partitions");
        manager = new PartitionManager(currentFile.getPath(), currentFolder.getPath());
    }

    @Test
    @DisplayName("Having no partition directory a new one should be created.")
    public void directoryShouldBeCreated() {
        try {
            manager.createDirectory();
            assertTrue(currentFolder.exists());
        } catch (IOException e) {
            fail("An error occurred trying to create the directory.", e);
        } finally {
            try {
                manager.ceasePartitionWorks();
            } catch (IOException e) {
                fail("Partitioner could not cease the work.", e);
            }
        }
    }

    @Test
    @DisplayName("Having a partition directory created, use one.")
    public void directoryShouldBeChecked() {
        try {
            manager.createDirectory();
            manager.createDirectory();
            assertTrue(currentFolder.exists());

        } catch (IOException e) {
            fail("An error occurred trying to create or remove directory.", e);
        } finally {
            try {
                manager.ceasePartitionWorks();
            } catch (IOException e) {
                fail("Partitioner could not cease the work.", e);
            }
        }
    }

    @Test
    @DisplayName("Having partition directory already created and filled with partitions, clear it.")
    public void directoryShouldBeCleared() {
        try {
            manager.createDirectory();

            var folderPath = manager.getPartitionDirectoryPath();

            var directoryContents = new File(folderPath + "/waiting.partition");
            directoryContents.createNewFile();

            manager.clearDirectory();
            assertEquals(Objects.requireNonNull(currentFolder.listFiles()).length, 0);

        } catch (IOException e) {
            fail("An error occurred when trying to create, remove or clear the directory.", e);
        } finally {
            try {
                manager.ceasePartitionWorks();
            } catch (IOException e) {
                fail("Partitioner could not cease the work.", e);
            }
        }
    }

    @Test
    @DisplayName("Having partition directory already created and filled with partitions, delete it.")
    public void directoryShouldBeDeleted() {
        try {
            manager.createDirectory();

            var folderPath = manager.getPartitionDirectoryPath();

            var directoryContents = new File(folderPath + "/waiting.partition");
            directoryContents.createNewFile();

            manager.ceasePartitionWorks();
            assertFalse(currentFolder.exists());

        } catch (IOException e) {
            fail("An error occurred when trying to create, remove or clear the directory.", e);
        }
    }
}
