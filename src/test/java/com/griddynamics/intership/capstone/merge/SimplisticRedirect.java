package com.griddynamics.intership.capstone.merge;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;


/**
 * Contains most simple implementation of redirect for Partitioner.
 */
public class SimplisticRedirect {

    /**
     * May be used to test Partitioner base functionality.
     * @param reader Origin source.
     * @param writer Writer to redirect stream to.
     * @param partitionSize How great each partition is.
     * @return Whether "redirect" succeeded at filling the partition completely.
     * @throws IOException Throws the exception when trying to read or access source file and to create or delete contents of the partition folder.
     * @see com.griddynamics.intership.capstone.merge.Partitioner#redirect
     */
    public static boolean redirect(BufferedReader reader, BufferedWriter writer, int partitionSize) throws IOException {
        String line;

        var counter = 0;
        while (counter < partitionSize && (line = reader.readLine()) != null && line.length() != 0) {
            writer.write(line);
            writer.newLine();
            counter++;
        }

        writer.close();

        return counter == partitionSize;
    }

}
