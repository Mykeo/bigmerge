package com.griddynamics.intership.capstone.merge;

import java.io.*;
import java.util.Arrays;
import java.util.List;

/**
 * Is responsible for merging sorted files.
 */
public class Merger {

    private final Partitioner parted;

    /**
     * Constructs a Merger that keeps a Partitioner instance.
     * @param parted Partitioner instance.
     */
    public Merger(Partitioner parted) { this.parted = parted; }

    /**
     * Runs Merger instance.
     * Requires Partitioner instance to be passed to work.
     * Note that the Partitioner has to produce partition of sorted order.
     * Runs "runPartitioner" method to produce the results.
     * @throws IOException May be caused either by "runPartitioner" method from Partitioner class
     * or running merging algorithm in the partition folder.
     * @throws SecurityException When it is not possible to delete the partition files. Thrown by the merging algorithm.
     * @throws RuntimeException Is thrown when either no partition or an only partition is present.
     */
    public File runMerger() throws IOException {
        parted.runPartitioner();

        var files = fileNames();

        if (files.length == 0)
            throw new RuntimeException("No partitions to handle.");

        if (files.length == 1)
            throw new RuntimeException("No need to handle an only partition.");

        return merge(Arrays.asList(files));
    }

    /**
     * @return List of partitions.
     */
    private File[] fileNames() { return new File(parted.getPartitionDirectoryPath()).listFiles(); }

    /**
     * Creates a temporary partition for sorted input.
     * @return Temporary file.
     * @throws IOException In case you could not create a file or access it.
     */
    private File runTemporary() throws IOException {
        var target = new File(parted.getPartitionDirectoryPath());
        return File.createTempFile(
                parted.getOriginFileName() + ".",
                ".partition",
                target);
    }

    /**
     * Merges two files into one, keeping in mind that a partition may well exceed current memory limit.
     * @param leftFile Left partition.
     * @param rightFile Right partition.
     * @return New merged partition.
     * @throws IOException When it is not possible to create, read or write to the partition folder.
     * @throws SecurityException When it is not possible to delete the partition files.
     */
    private File mergeSortedFiles(File leftFile, File rightFile) throws IOException {

        var handle = runTemporary();

        var leftReader = new BufferedReader(new FileReader(leftFile));
        var rightReader = new BufferedReader(new FileReader(rightFile));
        var writer = new BufferedWriter(new FileWriter(handle));

        String leftVal = "";
        String rightVal = "";

        String line;

        // First input.
        if ((line = leftReader.readLine()) != null && line.length() != 0) {
            leftVal = line;
        }

        if ((line = rightReader.readLine()) != null && line.length() != 0) {
            rightVal = line;
        }

        // Both readers are not empty.
        boolean leftIsLess = leftVal.compareTo(rightVal) < 0;

        // The least value is popped immediately.
        var currentReader = leftIsLess ? leftReader : rightReader;

        // Starting up the wheelchair.
        if (leftIsLess)
            writer.write(leftVal);
        else
            writer.write(rightVal);
        writer.newLine();

        // Reading only.
        while ((line = currentReader.readLine()) != null && line.length() != 0) {

            // Extracting value.
            if (leftIsLess)
                leftVal = line;
            else
                rightVal = line;

            leftIsLess = leftVal.compareTo(rightVal) < 0;

            currentReader = leftIsLess ? leftReader : rightReader;

            // Supply values.
            if (leftIsLess)
                writer.write(leftVal);
            else
                writer.write(rightVal);
            writer.newLine();
        }

        // Deals with left-outs.
        if (leftIsLess) {
            // A value is preserved in a variable.
            writer.write(rightVal);
            writer.newLine();
            rightReader.transferTo(writer);
        }
        else {
            writer.write(leftVal);
            writer.newLine();
            leftReader.transferTo(writer);
        }

        // Cleaning up.
        leftReader.close();
        rightReader.close();
        writer.close();

        leftFile.delete();
        rightFile.delete();

        return handle;
    }

    /**
     * Does merging using "mergeSortedFiles" method.
     * @param files List of files to be used.
     * @return Merged file.
     * @throws IOException When it is not possible to create, read or write to the partition folder.
     */
    private File merge(List<File> files) throws IOException {
        if (files.size() > 1) {
            int inHalf = files.size() >> 1;
            var leftPart = files.subList(0, inHalf);
            var rightPart = files.subList(inHalf, files.size());

            return mergeSortedFiles(merge(leftPart), merge(rightPart));
        }
        else return files.get(0);
    }

}
