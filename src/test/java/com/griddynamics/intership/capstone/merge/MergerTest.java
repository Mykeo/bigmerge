package com.griddynamics.intership.capstone.merge;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class MergerTest {

    private static File currentFile;
    private static File currentFolder;
    private static Partitioner partitioner;

    @BeforeClass
    @DisplayName("Facilitates finding locations.")
    public static void createSourceFileAndPartitionDirectory() {
        currentFile = new File(FilesToList.testDirectory + "integers.random");
        currentFolder = new File(FilesToList.testDirectory + "integers.random.partitions");
    }

    @Test
    @DisplayName("Runs merger, which reduces file count to one.")
    public void runningMergerShouldReduceFileCount() {
        try {
            partitioner = new SortedPartitioner(5,
                    currentFile.getPath(),
                    currentFolder.getPath());
            Merger merger = new Merger(partitioner);
            merger.runMerger();

            var files = currentFolder.listFiles();

            assert files != null;
            if (files.length != 1) {
                fail("Merger must reduce file count to 1.");
            }

            var l = FilesToList.readFileByLine(files[0]);
            boolean success = true;

            int counter = 1;
            while (counter < l.size() && success) {
                success = l.get(counter - 1).compareTo(l.get(counter)) <= 0;
                counter++;
            }

            assertTrue(success);
        } catch (IOException e) {
            fail("Merger threw exception while running or while the test attempting to" +
                    " compare the result", e);
        } finally {
            try {
                partitioner.ceasePartitionWorks();
            } catch (IOException e) {
                fail("Partitioner could not cease the work.", e);
            }
        }
    }

}
