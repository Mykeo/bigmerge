package com.griddynamics.intership.capstone.merge;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

public class MergedPartitionerTest {

    private static File currentFile;
    private static File currentFolder;
    private static File resultFile;
    private static Partitioner partitioner;

    @BeforeClass
    @DisplayName("Creates partition manager in the current folder.")
    public static void createPartitionManager() {
        currentFile = new File(FilesToList.testDirectory + "integers.random");
        currentFolder = new File(FilesToList.testDirectory +  "integers.random.partitions");
        resultFile = new File(FilesToList.testDirectory +  "integers.result");
        partitioner = new SortedPartitioner(5,
                currentFile.getPath(),
                currentFolder.getPath());
    }

    @Test
    @DisplayName("Having no partition directory a new one should be created.")
    public void directoryShouldBeCreated() {
        try {
            var mergedPartitioner = new MergedPartitioner(partitioner, resultFile.getPath());
            mergedPartitioner.runMergedPartitioner();
            assertTrue(resultFile.exists());
        } catch (IOException e) {
            fail("An error occurred trying to create the directory.", e);
        } finally {
            try {
                partitioner.ceasePartitionWorks();
                resultFile.delete();
            } catch (IOException e) {
                fail("Partitioner could not cease the work.", e);
            }
        }
    }

}
