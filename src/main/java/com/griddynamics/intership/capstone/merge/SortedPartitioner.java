package com.griddynamics.intership.capstone.merge;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Divides up partitions in a sorted order.
 */
public final class SortedPartitioner extends Partitioner {

    /**
     * Will create a SortedPartitioner object.
     * @param partitionSize How many lines does a partition will contain.
     * @param originFileCompletePath Complete path to source file to be partitioned.
     * @param partitionDirectoryCompletePath Complete path to a directory that will contain partitions.
     */
    public SortedPartitioner(int partitionSize, String originFileCompletePath, String partitionDirectoryCompletePath) {
        super(partitionSize, originFileCompletePath, partitionDirectoryCompletePath);
    }


    /**
     * Will create a SortedPartitioner object.
     * @param partitionSize How many lines does a partition will contain.
     * @param originFilePath Path of a file to be partitioned.
     * @param originFileName The file's name.
     * @param partitionDirectoryPath Directory path that will contain partitions.
     * @param partitionDirectoryName The directory's name.
     */
    public SortedPartitioner(int partitionSize, String originFilePath, String originFileName, String partitionDirectoryPath, String partitionDirectoryName) {
        this(partitionSize,
                originFilePath + "/" + originFileName,
                partitionDirectoryPath + "/" + partitionDirectoryName);
    }

    /**
     * Will redirect input from origin source to a partition till it fills completely, or the origin source gets emptied.
     * Note that the resulting partitions are kept in ascending order, sorted by their String values.
     * Closes writer when finished.
     * @param writer Writer to redirect stream to.
     * @param reader Origin source.
     * @return Whether "redirect" succeeded at filling the partition completely with ordered input.
     * @throws IOException Throws the exception when trying to read or access
     * source file and to create or delete contents of the partition folder.
     */
    @Override
    protected boolean redirect(BufferedReader reader, BufferedWriter writer) throws IOException {
        String line;

        var content = new ArrayList<String>();

        while (content.size() < getPartitionSize() && (line = reader.readLine()) != null && line.length() != 0) {
            content.add(line);
        }

        Collections.sort(content);

        for (String val : content) {
            writer.write(val);
            writer.newLine();
        }

        writer.close();

        return content.size() == getPartitionSize();
    }
}
