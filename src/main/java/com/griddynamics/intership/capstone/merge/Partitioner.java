package com.griddynamics.intership.capstone.merge;

import java.io.*;

/**
 * Splits a heavy file into processable partitions.
 */
public abstract class Partitioner {

    private final int partitionSize;
    protected final PartitionManager manager;

    /**
     * Will create partitioner object.
     * @param partitionSize How many lines does a partition will contain.
     * @param originFileCompletePath Complete path to source file to be partitioned.
     * @param partitionDirectoryCompletePath Complete path to a directory that will contain partitions.
     */
    public Partitioner(int partitionSize, String originFileCompletePath, String partitionDirectoryCompletePath) {
        this.partitionSize = partitionSize;
        manager = new PartitionManager(originFileCompletePath, partitionDirectoryCompletePath);
    }

    /**
     * Will create partitioner object.
     * @param partitionSize How many lines does a partition will contain.
     * @param originFilePath Path of a file to be partitioned.
     * @param originFileName The file's name.
     * @param partitionDirectoryPath Directory path that will contain partitions.
     * @param partitionDirectoryName The directory's name.
     */
    public Partitioner(int partitionSize, String originFilePath, String originFileName, String partitionDirectoryPath, String partitionDirectoryName) {
        this(partitionSize,
                originFilePath + "/" + originFileName,
                partitionDirectoryPath + "/" + partitionDirectoryName);
    }

    /**
     * Runs partitioner main functionality. Requires "redirect" method to be implemented.
     * @throws IOException Throws the exception when trying to read or access
     * source file and to create or delete contents of the partition folder and deletion of the partition folder itself.
     */
    public void runPartitioner() throws IOException {
        manager.createDirectory();
        manager.clearDirectory();
        var handle = fileReader();

        int currentPartition = 0;
        var partitionHandle = fileWriter(currentPartition);
        while (redirect(handle, partitionHandle))
            partitionHandle = fileWriter(++currentPartition);
        handle.close();
    }

    /**
     * Creates a reader to access the origin file.
     * @return Buffered reader to origin file.
     * @throws IOException For some reason the source file cannot be accessed to.
     */
    private BufferedReader fileReader() throws IOException {
        return new BufferedReader(new FileReader(manager.getOriginFileName()));
    }

    /**
     * Will create a partition given partition number.
     * @param partitionNumber Partition number.
     * @return Buffered reader to write into a partition.
     * @throws IOException For some reason cannot create or write to a partition file.
     */
    private BufferedWriter fileWriter(int partitionNumber) throws IOException {
        var originFullName = manager.getOriginFileName().split("/");
        String partitionName = manager.getPartitionDirectoryPath() + "/" + originFullName[originFullName.length - 1] + "." + partitionNumber + ".partition";
        return new BufferedWriter(new FileWriter(partitionName));
    }

    /**
     * Will redirect input from origin source to a partition till it fills completely, or the origin source gets emptied.
     * Closes writer when finished.
     * @param writer Writer to redirect stream to.
     * @param reader Origin source.
     * @returns Whether "redirect" succeeded at filling the partition completely.
     * @throws IOException Throws the exception when trying to read or access
     * source file and to create or delete contents of the partition folder.
     */
    protected abstract boolean redirect(BufferedReader reader, BufferedWriter writer) throws IOException;

    /**
     * @return Source file name.
     */
    public String getOriginFileName() { return manager.getOriginFileName(); }

    /**
     * @return Directory path where all partitions are kept.
     */
    public String getPartitionDirectoryPath() { return manager.getPartitionDirectoryPath(); }

    /**
     * @return Partition size limit.
     */
    public int getPartitionSize() { return partitionSize; }

    /**
     * In case you're done working, call this method to properly delete the partition folder.
     * @throws IOException In case it was not possible to delete a file in the folder or the folder itself.
     */
    public void ceasePartitionWorks() throws IOException {
        manager.ceasePartitionWorks();
    }

}
