package com.griddynamics.intership.capstone.merge;

import java.io.File;
import java.io.IOException;

public class Start {

    public static void main(String[] args) {
        var p = new SortedPartitioner(7,
                "integers.random",
                "integers.random.partitions");
        var mp = new MergedPartitioner(p, "integers.result");
        try {
            mp.runMergedPartitioner();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
