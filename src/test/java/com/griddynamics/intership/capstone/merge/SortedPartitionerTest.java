package com.griddynamics.intership.capstone.merge;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import static org.junit.jupiter.api.Assertions.*;

public class SortedPartitionerTest {

    private static File currentFile;
    private static File currentFolder;
    private static Partitioner partitioner;

    @BeforeClass
    @DisplayName("Creates files for sorted partitioner to work in the current folder.")
    public static void createSourceFileAndPartitionDirectory() {
        currentFile = new File(FilesToList.testDirectory + "integers.random");
        currentFolder = new File(FilesToList.testDirectory + "integers.random.partitions");
    }

    @Test
    @DisplayName("Runs sorted partitioner, which partition size is greater than the file itself.")
    public void runningSortedPartitionerShouldProduceSortedPartitions() {
        try {
            partitioner = new SortedPartitioner(5,
                    currentFile.getPath(),
                    currentFolder.getPath());
            partitioner.runPartitioner();

            var files = currentFolder.listFiles();
            assert files != null;
            var fileStream = Arrays.stream(files);
            var filteredStream = fileStream.map(FilesToList::readFileByLine).filter(l -> {
                boolean success = true;

                int counter = 1;
                while (counter < l.size() && success) {
                    success = l.get(counter - 1).compareTo(l.get(counter)) <= 0;
                    counter++;
                }

                return success;
            });

            var onStart = files.length;
            var onFinish = filteredStream.count();
            assertEquals(onStart, onFinish);
        } catch (IOException e) {
            fail("Sorted partitioner threw exception while running or while the test attempting to" +
                    " compare the result", e);
        } finally {
            try {
                partitioner.ceasePartitionWorks();
            } catch (IOException e) {
                fail("Partitioner could not cease the work.", e);
            }
        }
    }

}
