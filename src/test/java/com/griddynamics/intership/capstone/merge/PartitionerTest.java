package com.griddynamics.intership.capstone.merge;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

public class PartitionerTest {

    private static File currentFile;
    private static File currentFolder;
    private static Partitioner partitioner;

    @BeforeClass
    @DisplayName("Creates files that point to the source file and the partition directory.")
    public static void createSourceFileAndPartitionDirectory() {
        currentFile = new File(FilesToList.testDirectory + "integers.random");
        currentFolder = new File(FilesToList.testDirectory + "integers.random.partitions");
    }

    @Test
    @DisplayName("Runs partitioner, which partition size is greater than the file itself. Uses simplistic redirect implementation.")
    public void runningOversizedPartitionerShouldProduceAnOnlyPartition() {
        try {
            partitioner = new Partitioner(500,
                    currentFile.getPath(),
                    currentFolder.getPath()) {
                @Override
                protected boolean redirect(BufferedReader reader, BufferedWriter writer) throws IOException {
                    return SimplisticRedirect.redirect(reader, writer, getPartitionSize());
                }
            };
            partitioner.runPartitioner();

            var listing = currentFolder.listFiles();
            var total = Objects.requireNonNull(listing).length;

            if (total == 0 || total > 1) {
                fail("An only partition must be produced");
            }
        } catch (IOException e) {
            fail("Sorted partitioner threw exception while running or while the test attempting to" +
                    " compare the result", e);
        } finally {
            try {
                partitioner.ceasePartitionWorks();
            } catch (IOException e) {
                fail("Partitioner could not cease the work.", e);
            }
        }
    }

    @Test
    @DisplayName("Partitioner with small partition size must produce multiple partitions. Uses simplistic redirect implementation")
    public void runningTinyPartitionerShouldProduceMultiplePartition() {
        try {
            partitioner = new Partitioner(5,
                    currentFile.getPath(),
                    currentFolder.getPath()) {
                @Override
                protected boolean redirect(BufferedReader reader, BufferedWriter writer) throws IOException {
                    return SimplisticRedirect.redirect(reader, writer, getPartitionSize());
                }
            };
            partitioner.runPartitioner();

            var listingSize = Objects.requireNonNull(currentFolder.listFiles()).length;

            var originByLineSize = FilesToList.readFileByLine(currentFile.getPath()).size();
            var incompletePartitionCount = originByLineSize / partitioner.getPartitionSize();
            var properPartitionCount = incompletePartitionCount + (
                    incompletePartitionCount % partitioner.getPartitionSize() > 0 ? 1 : 0
                    );

            if (listingSize != properPartitionCount) {
                fail(properPartitionCount + " must be produced.");
            }
        } catch (IOException e) {
            fail("Partitioner threw exception while running or while the test attempting to" +
                    " compare the result", e);
        } finally {
            try {
                partitioner.ceasePartitionWorks();
            } catch (IOException e) {
                fail("Partitioner could not cease the work.", e);
            }
        }
    }

}
