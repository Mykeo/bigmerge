package com.griddynamics.intership.capstone.merge;

import java.io.File;
import java.io.IOException;

/**
 * Runs Merger over given Partitioner and saves the results.
 * Deals with saving the results and proper closing.
 */
public class MergedPartitioner {

    private final Partitioner parted;
    private final Merger merger;
    private final String outputFileCompletePath;

    /**
     * Produces MergedPartitioner instance.
     * @param parted Partitioner over which a Merger will be run.
     * @param outputFileCompletePath Complete path.
     */
    public MergedPartitioner(Partitioner parted, String outputFileCompletePath) {
        this.parted = parted;
        this.merger = new Merger(parted);
        this.outputFileCompletePath = outputFileCompletePath;
    }

    /**
     * Produces MergedPartitioner instance.
     * @param parted Partitioner over which a Merger will be run.
     * @param outputFilePath File path.
     * @param outputFileName Output file name.
     */
    public MergedPartitioner(Partitioner parted, String outputFilePath, String outputFileName) {
        this(parted, outputFilePath + "/" + outputFileName);
    }

    /**
     * Runs MergedPartitioner.
     * That involves creation of the partition folder and its contents, running merger and partitioner,
     * replacing result file and deletion of the partition folder.
     * @throws IOException In case file could not be created, deleted or replaced, or the partition directory could not be deleted.
     * @throws SecurityException May be cause by running merger instance.
     * @throws RuntimeException Is thrown when either no partition or an only partition is present.
     */
    public void runMergedPartitioner() throws IOException {

        var target = new File(outputFileCompletePath);
        if (!merger.runMerger().renameTo(target))
            throw new IOException("File could not be moved to: " + target.getAbsolutePath());

        parted.ceasePartitionWorks();
    }

}
