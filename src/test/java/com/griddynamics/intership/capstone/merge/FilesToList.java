package com.griddynamics.intership.capstone.merge;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * A utility class that contains reading methods.
 */
public class FilesToList {

    /**
     * Return file contents in a list, line by line.
     * @param file The file to be worked on.
     * @return Contents of the file kept in a list, line by line.
     * @throws RuntimeException When it was not possible to read the file or the file is not present at the given location.
     * Note that it contains IOException in it.
     */
    public static List<String> readFileByLine(File file) {
        try {
            var reader = new BufferedReader(new FileReader(file));

            String byLine;
            var container = new ArrayList<String>();
            while ((byLine = reader.readLine()) != null && byLine.length() > 0)
                container.add(byLine);

            return container;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Return file contents in a list, line by line.
     * @param fileLocation Location where the file is being kept.
     * @return Contents of the file kept in a list, line by line.
     * @throws IOException When it was not possible to read the file or the file is not present at the given location.
     * Note that it is wrapped into RuntimeException.
     */
    public static List<String> readFileByLine(String fileLocation) {
        return readFileByLine(new File(fileLocation));
    }

    public static final String testDirectory = "src/test/resources/";
}
