package com.griddynamics.intership.capstone.merge;

import java.io.File;
import java.io.IOException;

/**
 * Manages partitioning for Partitioner class.
 */
public class PartitionManager {

    private final String originFileName;
    private final String partitionDirectoryPath;

    /**
     * Will create partition manager object.
     * @param originFileCompletePath Complete path to source file to be partitioned.
     * @param partitionDirectoryCompletePath Complete path to a directory that will contain partitions.
     */
    public PartitionManager(String originFileCompletePath, String partitionDirectoryCompletePath) {
        this.originFileName = originFileCompletePath;
        this.partitionDirectoryPath = partitionDirectoryCompletePath;
    }

    /**
     * Will create partition manager object.
     * @param originFilePath Path of a file to be partitioned.
     * @param originFileName The file's name.
     * @param partitionDirectoryPath Directory path that will contain partitions.
     * @param partitionDirectoryName The directory's name.
     */
    public PartitionManager(String originFilePath, String originFileName, String partitionDirectoryPath, String partitionDirectoryName) {
        this(originFilePath + "/" + originFileName,
                partitionDirectoryPath + "/" + partitionDirectoryName);
    }

    /**
     * Creates a directory if one does not exist.
     * @throws IOException In case it was not possible to create the partition folder.
     */
    public void createDirectory() throws IOException {
        var target = new File(partitionDirectoryPath);
        if (!target.exists() && !target.mkdir())
            throw new IOException("It was not possible to create such a directory at: " + target.getAbsolutePath());
    }

    /**
     * Clears partition directory's contents.
     * @throws IOException In case it was not possible to delete a file.
     */
    public void clearDirectory() throws IOException {
        var files = new File(partitionDirectoryPath).listFiles();

        if (files != null)
            for (var file : files) {
                if (!file.delete())
                    throw new IOException("It was not possible to delete following file: " + file.getPath());
            }
    }

    /**
     * Helper method to clean up the workspace.
     * @throws IOException In case it was not possible to delete a file or the folder.
     */
    public void ceasePartitionWorks() throws IOException {
        clearDirectory();
        var target = new File(partitionDirectoryPath);
        if (target.exists() && !target.delete())
            throw new IOException("It was not possible to delete the folder at: " + target.getAbsolutePath());
    }

    /**
     * @return Source file name.
     */
    public String getOriginFileName() { return originFileName; }

    /**
     * @return Directory path where all partitions are kept.
     */
    public String getPartitionDirectoryPath() { return partitionDirectoryPath; }

}
